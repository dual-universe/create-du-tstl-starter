const childProcess = require("child_process");
const path = require("path");
const fs = require("fs");
const util = require("util");
const ts = require("typescript");

const exec = util.promisify(childProcess.exec);
const mkdirp = util.promisify(fs.mkdir);

const root = path.resolve(__dirname, "..");
const dist = path.resolve(root, "dist");
const WRAP = path.resolve(__dirname, "wrap.lua");

const pkg = require("../package.json");
const NAME = pkg.name || "bundle";
const VERSION = pkg.version;
const FILENAME = pkg.version ? `${NAME}_${VERSION}` : `${NAME}`

const readTsConfig = () => {
    const configFileName = ts.findConfigFile(
        "./",
        ts.sys.fileExists,
        "tsconfig.json"
    );
    return ts.readConfigFile(configFileName, ts.sys.readFile);
}

const getWrapConfig = () => {
    const config = readTsConfig();
    return Object.assign({
        output: "json",
        handleErrors: "none",
        fixGc: true,
        indentJson: false,
        callWithDot: true
    }, config["wrap"] || {});
}

const createFile = async (name) => {
    const stream = fs.createWriteStream(name);
    stream.write("");
    stream.close();
};

const runCommand = (cmd) => {
    return exec(cmd)
        .then(({stdout, stderr}) => {
            return stdout;
        })
};

const getLua = async () => {
    try {
        await runCommand("lua -v");
        return "lua";
    }
    catch (e) {}
    try {
        await runCommand("lua53 -v");
        return "lua53";
    }
    catch (e) {
        throw new Error("Lua not found. Please make lua available in your Path as `lua` or `lua53`");
    }
}

const toSlotAssignment = (slots) => {
    let assignments = [];
    for (let [k, v] of Object.entries(slots)) {
        assignments.push(`${k}:type=${v}`);
    }
    return assignments.join(" ");
};

const parseSlots = () => {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(root, "src/slots.d.ts"), "utf8", (err, file) => {

            const typeMap = {
                "DU.AntiGravityGenerator": "antigravityGenerator",
                "DU.Core": "core",
                "DU.Databank": "databank",
                "DU.Container": "fuelContainer",
                "DU.Industry": "industry",
                "DU.Gyro": "gyro",
                "DU.LaserDetector": "laserDetector",
                "DU.PressureTile": "pressable",
                "DU.Switch": "pressable",
                "DU.Radar": "radar",        // TODO differentiation between radar and PvP radar
                "DU.PvpRadar": "pvpRadar",
                "DU.Receiver": "receiver",
                "DU.Screen": "screen",
                "DU.Weapon": "weapon",
                "DU.DetectionZone": "enterable",
                "any": "anything",
            };
            const slots = {};
            file.match(/[^\r\n]+/g).forEach((line) => {
                if (line.startsWith("declare")) {
                    // (declare)( )(const)( )(slotName)(: )(slotType)
                    const match = line.match(/\w+\s+\w+\s+(\w+)\s*:\s*([A-Za-z_.]+)/);
                    if (!match[1] || !match[2]) {
                        throw new Error(`Malformed slot definition: '${line}'`)
                    }
                    const type = typeMap[match[2]];
                    if (!type) {
                        throw new Error(`Invalid slot type: '${match[2]}'`)
                    }
                    slots[match[1]] = type
                }
            });
            resolve(slots);
        });
    })
};

const buildYamlCommand = (lua, config, slotAssignment) => {
    // TODO YAML classes and how to handle "select=all"
    //   @selectAll annotation in slots.d.ts?
    /*
    When output format is YAML, "class" option can be used to link elements that do not have a type defined in this script, and the "select=all" option can be used to link all elements of that type or class.
Unit, system and library slots are added automatically. Slot names default to slot1, slot2, ..., slot10.
     */
    return `${lua} ${WRAP} ${dist}/bundle.lua ${dist}/${FILENAME}.conf --slots ${slotAssignment} --output=yaml`;
}

const buildJsonCommand = (lua, config, slotAssignment) => {
    let command = `${lua} ${WRAP} ${dist}/bundle.lua ${dist}/${FILENAME}.json --slots ${slotAssignment} --default-slot type=anything --output=json`;
    if (config.indentJson) {
        command += " --indent-json";
    }
    return command;
}

const buildCommand = (lua, config, slotAssignment) => {
    let command = config.output === "json" ? buildJsonCommand(lua, config, slotAssignment) : buildYamlCommand(lua, config, slotAssignment);
    if (config.fixGc) {
        command += " --fix-gc"
    }
    if (config.handleErrors === "min") {
        command += " --handle-errors-min";
    }
    else if (config.handleErrors === "full") {
        command += " --handle-errors"
    }

    if (!config.callWithDot) {
        command += ' --call-with ":"'
    }
    return command;
}
const noop = () => {}
const createDir = (pathname) => {
    // node is stupid with ensuring directories exist
    mkdirp(pathname)
        .then(noop)
        .catch((e) => {
            if (e.code === "EEXIST") {
                return;
            }
            throw e;
        });
}

Promise.all([
    createDir(`${dist}`),
    getLua(),
    parseSlots()
])
    .then(([_,lua,slots]) => {
        const config = getWrapConfig();
        const slotAssignment = toSlotAssignment(slots);
        const command = buildCommand(lua, config, slotAssignment)
        return runCommand(command);
    })
    .then(() => {
        console.log("Done bundling");
        process.exit(0);
    })
    .catch((e) => {
        console.error(e);
        process.exit(1);
    });

