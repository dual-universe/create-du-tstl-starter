/**
 * This file is only for declaring slot names.
 * It is used for bundling using hdparm's wrap.lua.
 * it parses the syntax `declare const <name> : <type>`
 * any other `declare` will break it.
 * You can include interface definitions for any of the generic interfaces
 */
interface DBStore {
    greeting: string;
}

declare const core: DU.Core;
declare const screen1: DU.Screen;
declare const db: DU.Databank<DBStore>;
