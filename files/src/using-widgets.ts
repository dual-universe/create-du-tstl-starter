type SelectWidgetData<K extends DU.WidgetType> = {
    [DU.WidgetType.None]: {},
    [DU.WidgetType.Agg]: DU.Widget.Agg,
    [DU.WidgetType.Cockpit]: DU.Widget.Cockpit,
    [DU.WidgetType.ControlUnit]: DU.Widget.ControlUnit,
    [DU.WidgetType.Core]: DU.Widget.Core,
    [DU.WidgetType.Engine]: DU.Widget.Engine,
    [DU.WidgetType.FuelTank]: DU.Widget.FuelTank,
    [DU.WidgetType.Gauge]: DU.Widget.FuelTank,
    [DU.WidgetType.Gyro]: DU.Widget.Gyro,
    [DU.WidgetType.Periscope]: DU.Widget.Periscope,
    [DU.WidgetType.Radar]: DU.Widget.Radar,
    [DU.WidgetType.Text]: DU.Widget.Text,
    [DU.WidgetType.Title]: DU.Widget.Title,
    [DU.WidgetType.Value]: DU.Widget.Value,
    [DU.WidgetType.Weapon]: DU.Widget.Weapon
}[K];

export const typesafeWidgetBuilder = <K extends DU.WidgetType>(kind: K, name: string, initialData: SelectWidgetData<K>) => {
    const panelId = system.createWidgetPanel(name);
    const widgetId = system.createWidget(panelId, kind);
    const dataId = system.createData(json.encode(initialData));
    system.addDataToWidget(dataId, widgetId);
    return (data: SelectWidgetData<K>) => {
        system.updateData(dataId, json.encode(data));
    }
}
