// Current Webstorm version is being disagreeable for some autocompletes.
// Need to explicitly add ref paths to allow for switch-completion over enums.
///<reference path="../node_modules/du-ts-types/du-api.d.ts"/>
///<reference path="../node_modules/du-ts-types/du-globals.d.ts"/>

/**
 * At least one import statement is needed to prevent TSTL from compiling top-level variables as globals.
 * Typically, a sizeable app will have imports, but for very simple scripts that are single files,
 * you need to add one pointless import. If you import one of the built-ins, no code will be generated e.g.
 *
 *  import * as vec3 from "cpml.vec3"
 *
 * See: https://github.com/TypeScriptToLua/TypeScriptToLua/issues/564
 */
import { typesafeWidgetBuilder } from "./using-widgets";

const updateTextWidget = typesafeWidgetBuilder(DU.WidgetType.Text, "Some Text", { text: "Hello World!" });

function setHTML(greeting: string) {
    screen1.clear();
    screen1.setHTML(`
    <style>
    .greeting {
        font-size: 6vh;
    }
    </style>
    <div class="greeting">${greeting}</div>
    `);
}

/**
 * Timers
 */
const enum Timers {
    UpdateScreen = "update_screen",
    UpdateWidget = "update_widget"
}

/**
 * Emitter channels. If you don't have an emitter, you can exclude Channels in the `script` declaration
 */
const enum Channels {
    SomeChannel = "some_channel"
}

/**
 * `declare` allows `script` to compile as a global and play well with wrap.lua
 */
declare let script: Script<Timers, Channels>;
// declare let script: Script<Timers>
script = {} as Script;

/**
 * Script.onStart runs on unit.start, and must be manually called at the bottom of this file
 */
script.onStart = () => {
    unit.setTimer(Timers.UpdateScreen, 1);
    unit.setTimer(Timers.UpdateWidget, 2);
    const greeting = "Hello Alioth!";
    db.setStringValue("greeting", greeting);
    setHTML(greeting);
};


script.onActionStart = (action: DU.Action) => {
    if (action === DU.Action.Opt1) {
        /**
         * option1 will change the greeting
         */
        db.setStringValue("greeting", "Goodbye Alioth!");
    }
};

let count = 1;
script.onTick = (timerId: Timers) => {
    if (timerId === Timers.UpdateScreen) {
        const greeting = db.getStringValue("greeting");
        setHTML(greeting);
    }
    else if (timerId === Timers.UpdateWidget) {
        count += 1;
        updateTextWidget({ text: "Hello World! " + count})
    }
};

/**
 * Start up the script
 */
script.onStart();
