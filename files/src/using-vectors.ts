/**
 * Some notes on dealing with vectors
 *
 * JS doesn't have operator overloading. Vectors are defined as an intersection type of the
 * vector object and a number. When using an operator, it infers that it's exclusively a
 * number, so you need to re-cast to Vector
 * see: https://typescripttolua.github.io/docs/advanced/writing-declarations#operator-overloads
 */
import type { Vector } from "cpml/vec3";
import * as vec3       from "cpml/vec3";

// lua-types like `math` are available in the global scope
const atan = math.atan;

interface Orientation {
    up: Vector;
    right: Vector;
    forward: Vector;
}

/**
 * Signed angle in radians between two vectors projected on a plane
 */
const signedRotationAngle = (normal: Vector, a: Vector, b: Vector): number => {
    return atan(a.cross(b).dot(normal), a.dot(b));
};

const getOrientation = (core: DU.Core): Orientation => {
    return {
        forward: vec3(core.getConstructWorldOrientationForward()),
        right: vec3(core.getConstructWorldOrientationRight()),
        up: vec3(core.getConstructWorldOrientationUp()),
    };
};

const getPitch = (vertical: Vector, forward: Vector): number => {
    const horizonForward = forward.project_on_plane(vertical);
    if (vertical.dot(forward) < 0) {
        return -forward.angle_between(horizonForward);
    }
    return forward.angle_between(horizonForward);
};

const getRoll = (vertical: Vector, state: Orientation) => {
    const horizonForward = state.forward.project_on_plane(vertical).normalize();
    const horizonRight = horizonForward.cross(vertical).normalize();
    return signedRotationAngle(horizonForward, horizonRight, state.right);
};

export const playWithVectors = (core: DU.Core) => {
    // because we're negating it, TS then infers a number. We need to cast back to Vector
    const vertical = -vec3(core.getWorldVertical()) as Vector;
    const orient = getOrientation(core);
    const pitch = getPitch(vertical, orient.forward);
    const roll = getRoll(vertical,orient);
};
