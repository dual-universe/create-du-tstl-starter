/**
 * deserializing JSON
 */
export const deserialize = (core: DU.Core): DU.Core.Data => {
    // dkjson is available in the global scope as `json`

    // Element interfaces have a namespaced `Data` object that corresponds to their
    // JSON data shape.
    const coreDataStr = core.getData();
    return json.decode<DU.Core.Data>(coreDataStr);
}
