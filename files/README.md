Typescript starter project for Dual Universe
--

`npm run build` will output `dist/name_version.lua` and `dist/name_version.json`. The json file can be pasted into a control unit.

Configuration for wrap.lua can be done in the tsconfig.json file.

Thanks to hdparm for `wrap.lua`.

Resources:
- [TypescriptToLua](https://typescripttolua.github.io/)
- [wrap.lua](https://board.dualthegame.com/index.php?/topic/20161-lua-tool-script-packagerconfigurator-wraplua/)
- [DualUniverse Gitlab](https://gitlab.com/dual-universe)

