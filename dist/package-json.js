"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildPackageJson = void 0;
// TODO temporarily downgraded tstl to `0.34.x`
//  when this is resolved, we can bump back to 0.35.x:
//  https://github.com/TypeScriptToLua/TypeScriptToLua/issues/915
exports.buildPackageJson = (name) => {
    return `
{
  "name": "${name}",
  "version": "0.0.1",
  "engines": {
    "node": ">=12.0.0"
  },
  "scripts": {
    "clean": "rimraf ./dist",
    "build:lua": "tstl",
    "build:json": "node ./scripts/build.js",
    "build": "npm run clean && npm run build:lua && npm run build:json"
  },
  "license": "MIT",
  "devDependencies": {
    "du-ts-types": "git+https://gitlab.com/Dual-Universe/ts-types.git",
    "lua-types": "^2.8.0",
    "rimraf": "^3.0.2",
    "typescript-to-lua": "^0.34.0"
  }
}
`;
};
