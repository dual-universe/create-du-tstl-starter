"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const util_1 = require("util");
const fs_extra_1 = require("fs-extra");
const path_1 = require("path");
const package_json_1 = require("./package-json");
const fs_1 = require("fs");
const exec = util_1.promisify(child_process_1.exec);
const fsMkdir = util_1.promisify(fs_1.mkdir);
const DEFAULT_NAME = "du-tstl-starter";
const WORKING_DIR = process.cwd();
const runShellCmd = async (command, opts) => {
    return exec(command, opts)
        .then(({ stdout, stderr }) => {
        return stdout.toString();
    })
        .catch((err) => {
        console.error("ERR: ", err.stderr);
        if (err.signal) {
            process.emit(err.signal, err.signal);
        }
        process.exit(err.code);
    });
};
const copyFiles = (appPath) => {
    console.log("Copying Files: STARTED");
    const localFilePath = path_1.resolve(__dirname, "..", "./files");
    return fs_extra_1.copy(localFilePath, appPath, {
        filter: (src, dest) => {
            // There isn't any callback available per-file, so we're hacking filter to log it.
            console.log(dest);
            return true;
        }
    })
        .then(() => {
        console.log("Copying Files: COMPLETE");
    })
        .catch((error) => {
        console.error("Copying Files: FAILED");
        throw error;
    });
};
const generateFiles = (name, appPath) => {
    const pkgJson = package_json_1.buildPackageJson(name);
    return new Promise((resolve, reject) => {
        console.log("Generating package.json: START");
        const stream = fs_1.createWriteStream(path_1.join(appPath, "package.json"));
        if (stream.write(pkgJson)) {
            stream.close();
        }
        else {
            stream.on("drain", () => {
                stream.close();
            });
        }
        stream.on("finish", () => {
            console.log("Generating package.json: COMPLETE");
            resolve();
        });
        stream.on("error", (e) => {
            console.error("Generating package.json: ERROR");
            reject(e);
        });
    });
};
const setup = async () => {
    const args = process.argv.slice(2);
    const name = args[0] || DEFAULT_NAME;
    const appPath = path_1.join(WORKING_DIR, name);
    return Promise.all([
        fsMkdir(path_1.join(appPath, "src"), { recursive: true }),
        fsMkdir(path_1.join(appPath, "scripts"), { recursive: true })
    ])
        .then(() => {
        return generateFiles(name, appPath);
    })
        .then(() => {
        return copyFiles(appPath);
    })
        .then(() => {
        return runShellCmd("npm install", {
            cwd: appPath
        })
            .then((stdout) => {
            console.log(stdout);
        });
    });
};
setup()
    .then(() => {
    console.log("Done!");
    process.exit(0);
})
    .catch((e) => {
    console.error(e);
    const code = e instanceof Error && typeof e.code === "number" ? e.code : 1;
    process.exit(code);
});
