import { exec as execCP, ExecException, ExecOptions }     from "child_process";
import { promisify }                                      from "util";
import { copy }                                           from "fs-extra";
import { resolve, join }                                  from "path";
import { buildPackageJson }                               from "./package-json";
import { mkdir, createWriteStream, MakeDirectoryOptions } from "fs";

const exec = promisify(execCP);
const fsMkdir = promisify<string, MakeDirectoryOptions>(mkdir);

const DEFAULT_NAME = "du-tstl-starter";
const WORKING_DIR = process.cwd();

interface ExecResult {
    stdout: string | Buffer;
    stderr: string | Buffer;
}

interface ShellException extends ExecException, ExecResult {}

const runShellCmd = async (command: string, opts?: ExecOptions): Promise<string> => {
    return exec(command, opts)
        .then(({ stdout, stderr }) => {
            return stdout.toString();
        })
        .catch((err: ShellException) => {
            console.error("ERR: ", err.stderr);
            if (err.signal) {
                process.emit(err.signal, err.signal);
            }
            process.exit(err.code);
        });
};

const copyFiles = (appPath: string): Promise<void> => {
    console.log("Copying Files: STARTED");
    const localFilePath = resolve(__dirname, "..", "./files");
    return copy(localFilePath, appPath, {
        filter: (src, dest) => {
            // There isn't any callback available per-file, so we're hacking filter to log it.
            console.log(dest);
            return true;
        }
    })
        .then(() => {
            console.log("Copying Files: COMPLETE");
        })
        .catch((error) => {
            console.error("Copying Files: FAILED");
            throw error;
        });
};

const generateFiles = (name: string, appPath: string): Promise<void> => {
    const pkgJson = buildPackageJson(name);
    return new Promise((resolve, reject) => {
        console.log("Generating package.json: START");
        const stream = createWriteStream(join(appPath, "package.json"));
        if (stream.write(pkgJson)) {
            stream.close();
        }
        else {
            stream.on("drain", () => {
                stream.close();
            });
        }

        stream.on("finish", (): void => {
            console.log("Generating package.json: COMPLETE");
            resolve();
        });
        stream.on("error", (e): void => {
            console.error("Generating package.json: ERROR");
            reject(e);
        });
    });
};


const setup = async (): Promise<void> => {
    const args = process.argv.slice(2);
    const name = args[0] || DEFAULT_NAME;
    const appPath = join(WORKING_DIR, name);
    return Promise.all([
            fsMkdir(join(appPath, "src"), { recursive: true }),
            fsMkdir(join(appPath, "scripts"), { recursive: true })
        ])
        .then(() => {
            return generateFiles(name, appPath);
        })
        .then(() => {
            return copyFiles(appPath);
        })
        .then(() => {
            return runShellCmd("npm install", {
                cwd: appPath
            })
                .then((stdout) => {
                    console.log(stdout);
                });
        });
};

setup()
    .then(() => {
        console.log("Done!");
        process.exit(0);
    })
    .catch((e: Error & { code: number }): void => {
        console.error(e);
        const code = e instanceof Error && typeof e.code === "number" ? e.code : 1;
        process.exit(code);
    });
